# Docker-Gitlab On-premises workshop

## Related Documentation 📄

[CI/CD Project reference on GitLab](https://gitlab.com/redcats002/gitlab-basic-workshop)

[Documentation in google docs form](https://docs.google.com/presentation/d/1zw1UzhqExACjtRHwN5-7IjCGP3Sy8f5QsuQHC4tP7KI/edit?usp=sharing)

[Official docs from Docker](https://docs.gitlab.com/ee/install/docker.html#install-gitlab-using-docker-engine)

## Overview

1. SETUP Docker installation on server [Docker-installation](/readme/readme_docker_installation.md)
1. [SERVER] SETUP SSH Keys [SSH-Keys](/readme/readme_ssh.md)
1. [GITLAB] SETUP Docker compose gitlab/registry [Docker-compose](/readme/readme_docker_compose.md)
1. [GITLAB] SETUP GitLab Runner [GitLab-Runner](/readme/readme_gitlab-runner.md)
1. [REGISTRY] SETUP Private container registry [Container-registry](/readme/readme_container_registry.md)
1. [GITLAB] TRY GitLab CI/CD [GitLab-CI/CD](/readme/readme_cicd.md)

## Pre-requisites

- Your GitLab Server (Only ip address it's ok)
- Your Deploy Server (Optional if you want to deploy to a different server)

## Installation commands 📃

Following commands are available:

### Check os-info

```bash
   cat /etc/os-release
```

## SETUP Docker on server

### Installing Docker

See how to install Docker
[Docker Installation commands](/readme/readme_docker_installation.md)

### Make user doesn't use sudo

RE-PASSWORD

```bash
sudo passwd ${USER}
```

check use port on local by

```bash
sudo lsof -i -P -n | grep LISTEN
```

- Give permission to docker groups (allow not sudo)

```bash
sudo usermod -aG docker ${USER}
```

will add sudo permissions to current User

To apply the new group membership, log out of the server and back in

```bash
su - ${USER}
```

Confirm that your user is now added to the docker group by typing:

```bash
groups
```

### Setup the volumes location

```bash
# For Linux -> /src/gitlab
export GITLAB_HOME=/srv/gitlab

# For macOS -> $HOME/gitlab
export GITLAB_HOME=$HOME/gitlab

# For Windows -> %cd%/gitlab
set GITLAB_HOME=%cd%/gitlab
```

### SETUP GitLab (using Docker Engine/Docker Compose)

**Using script to reduce the time to copy commands**

1. Create script file naming as anything such as "commands-build"

```bash
   # IN CASE OF DOCKER ENGINE (docker engine and set path at the same file)
   nano commands-build #1 for running / docker
   # IN CASE OF DOCKER COMPOSE (docker compose and set path are not the same files)
   nano commands-build #1 for running
   nano docker-compose.yml #2 for docker
```

2. Paste script to file

(Unix , linux, macOS)

- [Install by docker engine](/readme/readme_docker_engine.md)
- [Install by docker compose (recommend)](/readme/readme_docker_compose.md)

#### sshd port is already use

because default of sshd port is 22 and gitlab-ce trying to use sshd in port 22

[Look at the ssh config](/readme/readme_ssh.md)

**In this case we didn't want to map to DNS or hostname we use ip instead**

> Just config later after container is running

### Config using ip address instead of hostname in `gitlab.rb`

**What is Omnibus GitLab**

What is Omnibus GitLab is a way to package different services and tools required to run GitLab, so that most users can install it without laborious configuration.

[Omnibus-GitLab](https://docs.gitlab.com/omnibus/)

To change the external URL:

1. Optional. Before you change the external URL, determine if you have previously defined a custom Home page URL or After sign-out path. Both of these settings might cause unintentional redirecting after configuring a new external URL. If you have defined any URLs, remove them completely.

2. Edit `/etc/gitlab/gitlab.rb` and change external_url to your preferred URL:

**The `gitlab.rb` configuration can have vary path depend on your `docker -v` mapping to volume**

- to Shell to docker container `/etc/gitlab/gitlab.rb` path

```bash
   sudo docker exec -it gitlab bash
```

OR

```bash
   sudo docker exec -it gitlab editor /etc/gitlab/gitlab.rb
```

---

Adding external_url such as:

```yaml
external_url "http://gitlab.example.com"
```

3. Alternatively, you can use the IP address of your server:

```yaml
external_url "http://10.0.0.1"
```

4. In the previous examples we use plain HTTP. If you want to use HTTPS, see how to configure SSL.

Reconfigure GitLab: (Restart)

in gitlab docker container case:

```bash
   # bash to GitLab contianer
   docker exec -it gitlab bash

   # use gitlab-ctl to restart configuration (take a while to complete this)
   sudo gitlab-ctl reconfigure
```

(option) on top of docker case:

```bash
   sudo docker restart gitlab
```

See different configuration from original (while you in docker shell)

```bash
   gitlab-ctl diff-config
```

References:
[Configuration](https://docs.gitlab.com/ee/install/docker.html#configuration)
[CustomDomainName](https://stackoverflow.com/questions/26616833/omnibus-gitlab-on-ip-without-a-domain-name-and-with-custom-relative-url-root)
[OmnibusExternalUrl](https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-the-external-url-for-gitlab)

### Configure SSL for the GitLab Linux package

Enable HTTPS for the GitLab Linux package
Is a prerequisites to use Container registry
[GitLab-Registry](https://docs.gitlab.com/ee/administration/packages/container_registry.html)
[HTTPS:](https://docs.gitlab.com/omnibus/settings/ssl/index.html)

### Tracking process

The initialization process may take a long time. You can track this process with:

```bash
sudo docker logs -f gitlab
```

### Sign in with root

1. Visit the GitLab URL, and sign in with the username root and the password from the following command:

Windows/macOS

```bash
   sudo docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
```

2. Define your own password on url (password setting)

This `root` password will reset in 24hrs. So, please config this after get in to the gitlab

<!-- root@Wasd#1234 -->

> Note that The password file will be automatically deleted in the first reconfigure run after 24 hours.

### Pre-configure Docker container (Optional)

At Pre-configuration section

[Docker-engine](/readme/readme_docker_engine.md)
[Docker-compose](/readme/readme_docker_compose.md)

### Run GitLab on a public IP address

To expose GitLab on IP 198.51.100.1:

In case Docker engine (reference)

```bash
sudo docker run --detach \
--hostname gitlab.example.com \
--publish 198.51.100.1:443:443 \
--publish 198.51.100.1:80:80 \
--publish 198.51.100.1:22:22 \
--name gitlab \
--restart always \
--volume $GITLAB_HOME/config:/etc/gitlab \
--volume $GITLAB_HOME/logs:/var/log/gitlab \
--volume $GITLAB_HOME/data:/var/opt/gitlab \
--shm-size 256m \
gitlab/gitlab-ce:latest
```

## SETUP CICD

### SETUP GITLAB RUNNER

Full readme with commands in [GitLab-Runner Readme](/readme/readme_gitlab-runner.md)

#### Overview-gitlab-runner

1. Curl GitLab Runner on your machine
   [GitLab-Runner:](https://docs.gitlab.com/runner/install/index.html)
2. Install gitlab
   [GitLab-Runner-Linux:](https://docs.gitlab.com/runner/install/linux-repository.html)

3. Register

```bash
   gitlab-runner register
```

### Config GitLab Runner

`cat /etc/gitlab-runner/config.toml`

### CICD Variables

```yaml
SSH_PRIVATE_KEY
PROD_SERVER_IP = 159.223.81.231
APP_NAME
CI_REGISTRY_USER
CI_REGISTRY_PASSWORD
CI_REGISTRY_IMAGE
```

### SSH Keys configuration - For easy to do the following command

```bash
ssh-keygen -t rsa -C example@mail.com
```

### Private Container registry

Using docker registry image:

Visit this [README](/readme/readme_container_registry.md)

### (Optional for testing) Map localhost to hostname

```yaml
To make http://localwebapp:1234/ the same as http://localhost:1234/, edit the hosts file of your operating system by adding the line

# map gitlab.example to localhost
127.0.0.1 gitlab.example.com

The location of the hosts file depends on the operating system:

For UNIX-like operating systems, it's usually: `Users/etc/hosts`
For Windows, it's usually: `C:\Windows\System32\drivers\etc\hosts`

```

hosts (Windows)

```
# Copyright (c) 1993-2009 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handled within DNS itself.
# 127.0.0.1       localhost
# ::1             localhost
# Added by Docker Desktop
192.168.0.177 host.docker.internal
192.168.0.177 gateway.docker.internal
# To allow the same kube context to work on the host and the container:
127.0.0.1 kubernetes.docker.internal
# End of section

```

hosts (macOS)

```
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
##
127.0.0.1 localhost
255.255.255.255 broadcasthost
::1             localhost
# Added by Docker Desktop
# To allow the same kube context to work on the host and the container:
127.0.0.1 kubernetes.docker.internal

# map cm.gitlab.com to localhost
127.0.0.1 cm.gitlab.com


# End of section

```

### Testing

on our local point to gitlab-onpremises server (self-host)

1. Clone repository

- Create repository

`git clone http://gitlab.example.com/...`

- Try following command

`git add`

`git commit`

`git push`

`git pull`

2. Push to existing repository

```bash
# Create test.txt file
echo "HELLO WORLD" > test.txt

#init git to tracking folder
#.git will appear to folder

# check status that .git is work correctly
git status

# Add remote
git remote add origin [url]

# Create branch
git branch -M main

# add and commit
git add .
git commit -m “COMMIT NAME”

# Push to repository
git push –set-upstream origin main


```
