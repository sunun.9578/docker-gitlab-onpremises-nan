# Docker engine script

## Set path variables

```bash
# Linux
export GITLAB_HOME=/srv/gitlab
export DOCKER_REGISTRY=/srv/docker-registry
# Windows
set GITLAB_HOME=%cd%/gitlab
set DOCKER_REGISTRY=%cd%/docker-registry
```

## Docker engine

1. GitLab

1.1 Default configuration (GitLab)

for default configuration need to config the `gitlab.rb` later
on `/etc/gitlab/gitlab.rb` to adding `external_url` key

macOS

```bash
docker run --detach \
--hostname gitlab.example.com \
--publish 443:443 --publish 80:80 --publish 22:22 \
--name gitlab \
--restart always \
--volume $GITLAB_HOME/config:/etc/gitlab \
--volume $GITLAB_HOME/logs:/var/log/gitlab \
--volume $GITLAB_HOME/data:/var/opt/gitlab \
--shm-size 4096m \
gitlab/gitlab-ce:latest
```

Windows

```bash
docker run --detach \
--hostname gitlab.example.com \
--publish 443:443 --publish 80:80 --publish 22:22 \
--name gitlab \
--restart always \
--volume %GITLAB_HOME%/config:/etc/gitlab \
--volume %GITLAB_HOME%/logs:/var/log/gitlab \
--volume %GITLAB_HOME%/data:/var/opt/gitlab \
--shm-size 4096m \
gitlab/gitlab-ce:latest
```

1.2 Pre-configuration (GitLab)

```bash
docker run --detach \
--hostname gitlab.example.com \
--env GITLAB_OMNIBUS_CONFIG="external_url 'http://my.domain.com/'; gitlab_rails['lfs_enabled'] = true;" \
--publish 443:443 --publish 80:80 --publish 22:22 \
--name gitlab \
--restart always \
--volume $GITLAB_HOME/config:/etc/gitlab \
--volume $GITLAB_HOME/logs:/var/log/gitlab \
--volume $GITLAB_HOME/data:/var/opt/gitlab \
--shm-size 4096m \
gitlab/gitlab-ce:latest
```

2. Private registry

$IP_ADDRESS need to CHANGE to your server ip address

Windows

```bash
docker run -d \
    --name private_registry \
    --restart always \
    -p %IP_ADDRESS%:5000:5000 \
    -v $DOCKER_REGISTRY/data:/var/lib/registry \
    -e REGISTRY_HTTP_ADDR=0.0.0.0:5000 \
    registry:latest
```

macOS

```bash
docker run -d \
    --name private_registry \
    --restart always \
    -p $IP_ADDRESS:5000:5000 \
    -v $DOCKER_REGISTRY/data:/var/lib/registry \
    -e REGISTRY_HTTP_ADDR=0.0.0.0:5000 \
    registry:latest
```
