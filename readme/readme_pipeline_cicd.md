# GitLab CI/CD Pipeline

## Variables

```yaml
APP_NAME # Application name for deploy docker container
CI_REGISTRY_USER # Registry username for docker login
CI_REGISTRY_PASSWORD # Registry password for docker login
CI_REGISTRY_IMAGE # Registry image for pull, push image from private registry
CI_REGISTRY_ADDRESS # Registry address for docker
```

## CI Pipeline

```yaml
# Define Stages for ordering in script
stages:
  - build
  - test
  - docker-build
  - deploy

default:
  tags:
    - shared

# This folder is cached between builds
# https://docs.gitlab.com/ee/ci/yaml/index.html#cache
# cache:
#   paths:
#     - node_modules/

build:
  stage: build
  # Official framework image. Look for the different tagged releases at:
  # https://hub.docker.com/r/library/node/tags/
  image: node
  script:
    - echo "Start building App"
    - npm install
    - npm run build
    - echo "Build successfully!"
  artifacts:
    expire_in: 1 hour
    paths:
      - build
      - node_modules/

test:
  stage: test
  image: node
  script:
    - echo "Testing App-"
    - CI=true npm run lint
    - echo "Test successfully!"

docker-build:
  stage: docker-build
  image: docker:latest
  services:
    # - name: docker:24.0.3-dind
    - name: docker:dind
      alias: docker
  before_script:
    - docker logout
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY_ADDRESS
  script:
    - docker build -t "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE" . #CI_REGISTRY_IMAGE can look at docker hub of our repository
    # - docker scout quickview
    - docker push "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE"
deploy:
  stage: deploy
  image: docker
  services:
    - name: docker:dind
      alias: docker
  before_script:
    - docker logout
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY_ADDRESS
  script:
    - docker pull -t "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE"
    - docker stop $APP_NAME || true && docker rm $APP_NAME || true
    - docker run -p 4000:80 -d --name $APP_NAME "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE"
```

## Deploy

In Docker, the -p option is used to publish ports between the container and the host machine. The syntax for the -p option is as follows:

```bash
docker run -p <host_port>:<container_port> ...
```

<host_port>: This is the port number on the host machine (your local machine or server) that you want to expose and make accessible for incoming connections.

<container_port>: This is the port number inside the Docker container where the service is listening. This port will be mapped to the corresponding host port.

## Adding line notification to pipeline

Final `.gitlab-ci.yml`

For more information look at [line notification readme](/readme/readme_line_notify.md)

```yaml
# Define Stages for ordering in script
stages:
  - build
  - test
  - docker-build
  - deploy
  - clean_up
  - notify

default:
  tags:
    - shared

# This folder is cached between builds
# https://docs.gitlab.com/ee/ci/yaml/index.html#cache
# cache:
#   paths:
#     - node_modules/

build:
  stage: build
  # Official framework image. Look for the different tagged releases at:
  # https://hub.docker.com/r/library/node/tags/
  image: node
  only:
    - main
  script:
    - echo "Start building App"
    - npm install
    - npm run build
    - echo "Build successfully!"
  # artifacts:
  #   expire_in: 1 hour
  #   paths:
  #     - build
  #     - node_modules/

test:
  stage: test
  image: node
  only:
    - main
  script:
    - npm install
    - echo "Testing App-"
    - CI=true npm run lint
    - echo "Test successfully!"

docker-build:
  stage: docker-build
  image: docker:latest
  only:
    - main
  services:
    # - name: docker:24.0.3-dind
    - name: docker:dind
      alias: docker
  before_script:
    - docker logout $CI_REGISTRY_ADDRESS
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY_ADDRESS
  script:
    - docker build -t "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE" . #CI_REGISTRY_IMAGE can look at docker hub of our repository
    # - docker scout quickview
    - docker push "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE"

deploy:
  stage: deploy
  image: docker
  only:
    - main
  services:
    - name: docker:dind
      alias: docker
  before_script:
    - docker logout $CI_REGISTRY_ADDRESS
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY_ADDRESS
  script:
    - docker pull "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE"
    - docker stop $APP_NAME || true && docker rm $APP_NAME || true
    - docker run -p 4000:80 -d --name $APP_NAME "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE"

clean_up_job:
  stage: clean_up
  image: curlimages/curl:latest
  only:
    - main
  when: on_failure # trigger only when earlier stage has failed at least one job
  script:
    - echo "test"
  after_script: #in this keywords $CI_JOB_STATUS only available
    - 'curl --request POST --url https://notify-api.line.me/api/notify --header "Authorization: Bearer $LINE_CHANNEL_ACCESS_TOKEN" --header "Content-Type: application/x-www-form-urlencoded" --data message="[💥 Failed stage:$CI_JOB_STAGE - stage:$CI_JOB_NAME] Pipeline $CI_PIPELINE_IID for $CI_PROJECT_PATH ($CI_COMMIT_REF_NAME) was failed."'

notify-pipeline:
  stage: notify
  image: curlimages/curl:latest
  only:
    - main
  script:
    - echo "Line notification"
  when: always # always triggered
  after_script: #in this keywords $CI_JOB_STATUS only available
    - 'curl --request POST --url https://notify-api.line.me/api/notify --header "Authorization: Bearer $LINE_CHANNEL_ACCESS_TOKEN" --header "Content-Type: application/x-www-form-urlencoded" --data message="[🚀 Pipeline finished]   $CI_PIPELINE_IID for $CI_PROJECT_PATH ($CI_COMMIT_REF_NAME) has finished."'
```

## rules/workflow/only

### only:

So, in case you `.gitlab-ci.yml` triggered only in specific branch, you can edit you pipeline configuration (`.gitlab-ci.yml`)
by adding **only** keyword to every job

references: <https://docs.gitlab.com/ee/ci/yaml/#only--except>

example:

```yaml
deploy:
  stage: deploy
  image: docker
  only:
    - main # this job will trigger only in main branch
  services:
    - name: docker:dind
      alias: docker
  before_script:
    - docker logout $CI_REGISTRY_ADDRESS
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY_ADDRESS
  script:
    - docker pull "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE"
    - docker stop $APP_NAME || true && docker rm $APP_NAME || true
    - docker run -p 4000:80 -d --name $APP_NAME "$CI_REGISTRY_ADDRESS/$CI_REGISTRY_IMAGE"
```

### workflow:

`workflow:rules`
The rules keyword in `workflow` is similar to `rules` defined in jobs, but controls whether or not a **whole pipeline** is created.

When no `rules` evaluate to `true`, the pipeline does not run.

reference: <https://docs.gitlab.com/ee/ci/yaml/#workflow>

```yaml
# Define Stages for ordering in script
stages:
  - build
  - test
  - docker-build
  - deploy
  - clean_up
  - notify

workflow: # ADD THIS TO PIPELINE TRIGGER ONLY BRANCH MAIN
  rules:
    - if: $CI_COMMIT_BRANCH == "main"

default:
  tags:
    - shared

build:
  stage: build
  # ...
```
